// Copyright (C) 2022, California Institute of Technology and contributors
//
// This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
// If a copy of the MPL was not distributed with this file, You can obtain one at
// <https://mozilla.org/MPL/2.0/>.
//
// SPDX-License-Identifier: MPL-2.0

use std::sync::Mutex;

use once_cell::sync::Lazy;
use tokio::runtime;

use gst::glib;
use gst::prelude::*;
use gst::subclass::prelude::*;
use gst_base::prelude::*;
use gst_base::subclass::base_src::CreateSuccess;
use gst_base::subclass::prelude::*;

const DEFAULT_IS_LIVE: bool = false;

#[derive(Debug, Clone)]
struct Settings {
    server_url: String,
    channels: Vec<String>,
}

impl Default for Settings {
    fn default() -> Self {
        Settings {
            server_url: arrakis::DEFAULT_ARRAKIS_SERVER.to_string(),
            channels: Vec::new(),
        }
    }
}

#[derive(Debug, Default)]
pub struct ArrakisSrc {
    settings: Mutex<Settings>,
}

static CAT: Lazy<gst::DebugCategory> = Lazy::new(|| {
    gst::DebugCategory::new(
        "arrakissrc",
        gst::DebugColorFlags::empty(),
        Some("Arrakis source"),
    )
});

static RUNTIME: Lazy<runtime::Runtime> = Lazy::new(|| {
    runtime::Builder::new_multi_thread()
        .enable_all()
        .worker_threads(1)
        .build()
        .unwrap()
});

impl ObjectImpl for ArrakisSrc {
    fn properties() -> &'static [glib::ParamSpec] {
        static PROPERTIES: Lazy<Vec<glib::ParamSpec>> = Lazy::new(|| {
            vec![
                glib::ParamSpecString::new(
                    "server-url",
                    "Server URL",
                    "Arrakis server URL to connect to",
                    None,
                    glib::ParamFlags::READWRITE | gst::PARAM_FLAG_MUTABLE_READY,
                ),
                glib::ParamSpecBoxed::new(
                    "channels",
                    "Channels",
                    "Names of the channels to request",
                    Vec::<String>::static_type(),
                    glib::ParamFlags::READWRITE | gst::PARAM_FLAG_MUTABLE_READY,
                ),
                glib::ParamSpecBoolean::new(
                    "is-live",
                    "Is Live",
                    "Act like a live source",
                    DEFAULT_IS_LIVE,
                    glib::ParamFlags::READWRITE | gst::PARAM_FLAG_MUTABLE_READY,
                ),
            ]
        });

        PROPERTIES.as_ref()
    }

    fn set_property(
        &self,
        obj: &Self::Type,
        _id: usize,
        value: &glib::Value,
        pspec: &glib::ParamSpec,
    ) {
        let res = match pspec.name() {
            "server-url" => {
                let mut settings = self.settings.lock().unwrap();
                let url = value.get().expect("type checked upstream");
                settings.server_url = url;
                Ok(())
            }
            "channels" => {
                let mut settings = self.settings.lock().unwrap();
                let channels = value.get().expect("type checked upstream");
                settings.channels = channels;
                Ok(())
            }
            "is-live" => {
                let is_live = value.get().expect("type checked upstream");
                obj.set_live(is_live);
                Ok(())
            }
            _ => unimplemented!(),
        };

        if let Err(err) = res {
            gst::error!(
                CAT,
                obj: obj,
                "Failed to set property `{}`: {:?}",
                pspec.name(),
                err
            );
        }
    }

    fn property(&self, obj: &Self::Type, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
        match pspec.name() {
            "server-url" => {
                let settings = self.settings.lock().unwrap();
                settings.server_url.to_value()
            }
            "channels" => {
                let settings = self.settings.lock().unwrap();
                settings.channels.to_value()
            }
            "is-live" => obj.is_live().to_value(),
            _ => unimplemented!(),
        }
    }

    fn constructed(&self, obj: &Self::Type) {
        self.parent_constructed(obj);
        obj.set_automatic_eos(false);
        obj.set_format(gst::Format::Bytes);
    }
}

impl GstObjectImpl for ArrakisSrc {}

impl ElementImpl for ArrakisSrc {
    fn metadata() -> Option<&'static gst::subclass::ElementMetadata> {
        static ELEMENT_METADATA: Lazy<gst::subclass::ElementMetadata> = Lazy::new(|| {
            gst::subclass::ElementMetadata::new(
                "Arrakis Source",
                "Source/Network/Flight",
                "Read timeseries from Arrakis server",
                "Patrick Godwin <patrick.godwin@ligo.org>",
            )
        });

        Some(&*ELEMENT_METADATA)
    }
}

#[glib::object_subclass]
impl ObjectSubclass for ArrakisSrc {
    const NAME: &'static str = "ArrakisSrc";
    type Type = super::ArrakisSrc;
    type ParentType = gst_base::PushSrc;
    type Interfaces = (gst::URIHandler,);
}
